# Ode to Creative Commons

6-string, 8-string, HEJI, and sesqui scores will be released as soon as possible.

## Preface

The composer was inspired by exploring chord possibilities on the Kite guitar. Open strings are used extensively. This piece uses major, minor, augmented, and diminished triads, major and minor 7 and 9 chords, dominant 7, 9, b9, 7#11, and 9#11 chords, and fully diminished 7 chords. These chords are tuned with 7-limit Just Intonation ratios. The ratios for these chords are as simple as possible. The major triad is 4:5:6, the minor triad is 10:12:15, and the dominant 7 is 4:5:6:7. The more complex Pythagorean 3rds and 6ths are avoided. There are a few sections of natural harmonics. Square waves are approximated by plucking in the middle of the string where the octave harmonic lies.

This piece has been scored for 6-, 7- and 8-string Kite guitar. It has been notated in both up/down and pure HEJI. Adaptations are encouraged. The composer has performed this piece many times with differences from this score. The performer should feel free to make adaptations for their instrument and to simplify or expand parts for their own playing style. Though metronome markings are provided, these are only suggestions. The piece may be transposed. Distortion may also be used at any point. The performer is free to publish alternate versions of this piece under CC-BY-SA.

The composer believes musicians, artists, and researchers should have more control over the content they create. But they are too often forced to give away their rights to their works. In extreme cases such as John Fogerty's self-plagiarism suit, a composer may not even feel free to continue composing. If given a choice, not all content creators would agree that their works should be as restricted as the publishing and recording industries desire in order to maximize profits. Much of the composer's knowledge of the world comes from countless hours spent on Wikipedia. Creative Commons licenses such as CC-BY-SA allow sites like Wikipedia to exist. This starkly contrasts with JSTOR, a database that can't be freely shared with others. Creative Commons puts freedom in the hands of the composer, performer, and audience, and seeks to restrict only restrictions themselves.

This piece is dedicated to Aaron Swartz, and his dream of Open Access.

> Information is power. But like all power, there are those who want to keep it for themselves. The world's entire scientific and cultural heritage, published over centuries in books and journals, is increasingly being digitized and locked up by a handful of private corporations.

\- Aaron Swartz, Guerrilla Open Access Manifesto (2008)
